package com.br.mastertech.cartoes.repositories;

import com.br.mastertech.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao,Integer> {
}

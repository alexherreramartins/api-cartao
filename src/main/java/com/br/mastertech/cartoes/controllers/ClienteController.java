package com.br.mastertech.cartoes.controllers;

import com.br.mastertech.cartoes.models.Cliente;
import com.br.mastertech.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente createCliente(@RequestBody Cliente cliente ){
        return clienteService.createCliente(cliente);
    }

    @GetMapping("/{id}")
    public Optional<Cliente> createCliente(@PathVariable(name = "id") int id ){
        return clienteService.findClienteById(id);
    }
}

package com.br.mastertech.cartoes.DTOs;

import java.util.Date;

public class ResponseFaturaDTO {
    private int id;
    private double valorPago;
    private Date pagoEm;

    public ResponseFaturaDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public Date getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(Date pagoEm) {
        this.pagoEm = pagoEm;
    }
}

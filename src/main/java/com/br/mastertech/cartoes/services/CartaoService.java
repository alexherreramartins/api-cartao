package com.br.mastertech.cartoes.services;

import com.br.mastertech.cartoes.DTOs.RequestCartaoDTO;
import com.br.mastertech.cartoes.DTOs.ResponseCartaoDTO;
import com.br.mastertech.cartoes.DTOs.ResponseConsultaCartaoDTO;
import com.br.mastertech.cartoes.DTOs.StatusDTO;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.models.Cliente;
import com.br.mastertech.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;


    public Cartao createCartao(RequestCartaoDTO requestCartaoDTO){
        Cartao cartao = new Cartao();

        Optional<Cliente> cliente = clienteService.findClienteById(requestCartaoDTO.getClienteId());

        if (cliente.isPresent()){
            cartao.setCliente(cliente.get());
            cartao.setNumero(requestCartaoDTO.getNumero());
            cartao.setAtivo(false);

            return cartaoRepository.save(cartao);
        }
        throw new RuntimeException("Cliente não cadastrado!");

    }


    public ResponseCartaoDTO updateStatusCartao(StatusDTO statusDTO, int id) {
        Optional<Cartao> cartaoDB = cartaoRepository.findById(id);
        ResponseCartaoDTO responseCartaoDTO = new ResponseCartaoDTO();

        if (cartaoDB.isPresent()){
            cartaoDB.get().setAtivo(statusDTO.isAtivo());

            Cartao cartao = cartaoRepository.save(cartaoDB.get());

            responseCartaoDTO.setId(cartao.getId());
            responseCartaoDTO.setNumero(cartao.getNumero());
            responseCartaoDTO.setAtivo(cartao.isAtivo());
            responseCartaoDTO.setClienteId(cartao.getCliente().getId());
            return responseCartaoDTO;
        }
        throw new RuntimeException("Cartao não enontrado!");
    }

    public ResponseConsultaCartaoDTO findCartaoByIdDTO(int id) {
        ResponseConsultaCartaoDTO responseConsultaCartaoDTO = new ResponseConsultaCartaoDTO();

        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (cartao.isPresent()){
            responseConsultaCartaoDTO.setId(cartao.get().getId());
            responseConsultaCartaoDTO.setNumero(cartao.get().getNumero());
            responseConsultaCartaoDTO.setClienteId(cartao.get().getCliente().getId());

            return responseConsultaCartaoDTO;
        }
        throw new RuntimeException("Cartao não existe!");

    }

    public Optional<Cartao> findCartaoById(int id){
        return cartaoRepository.findById(id);
    }
}

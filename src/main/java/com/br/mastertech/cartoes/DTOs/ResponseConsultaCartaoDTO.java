package com.br.mastertech.cartoes.DTOs;

public class ResponseConsultaCartaoDTO {

    private int id;
    private String numero;
    private int clienteId;

    public ResponseConsultaCartaoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}

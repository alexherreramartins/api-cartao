package com.br.mastertech.cartoes.services;

import com.br.mastertech.cartoes.DTOs.RequestPagamentoDTO;
import com.br.mastertech.cartoes.DTOs.ResponseConsultaCartaoDTO;
import com.br.mastertech.cartoes.DTOs.ResponsePagamentoDTO;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.models.Cliente;
import com.br.mastertech.cartoes.models.Pagamento;
import com.br.mastertech.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;


    public ResponsePagamentoDTO createPagamento(RequestPagamentoDTO  requestPagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        ResponsePagamentoDTO responsePagamentoDTO = new ResponsePagamentoDTO();

        Optional<Cartao> cartao = cartaoService.findCartaoById(requestPagamentoDTO.getCartao_id());

        if (cartao.isPresent()) {
            if (cartao.get().isAtivo()) {
                pagamento.setCartao(cartao.get());
                pagamento.setDescricao(requestPagamentoDTO.getDescricao());
                pagamento.setValor(requestPagamentoDTO.getValor());

                Pagamento pagamentoDB = pagamentoRepository.save(pagamento);

                responsePagamentoDTO.setCartao_id(cartao.get().getId());
                responsePagamentoDTO.setDescricao(pagamentoDB.getDescricao());
                responsePagamentoDTO.setId(pagamentoDB.getId());
                responsePagamentoDTO.setValor(pagamentoDB.getValor());

                return responsePagamentoDTO;
            }
            throw new RuntimeException("Cartao esta inativo.");
        }
        throw new RuntimeException("Cartao nao cadastrado para pagamento.");
    }

    public Iterable<ResponsePagamentoDTO> findPagamentosIdCartao(int id_cartao) {

        List<ResponsePagamentoDTO> responsePagamentoDTOList = new ArrayList<>();

        List<Pagamento> pagamentoList = findAllByCartao(id_cartao);

        if (pagamentoList.size() > 0){
            for (Pagamento pagamento : pagamentoList) {
                ResponsePagamentoDTO responsePagamentoDTO = new ResponsePagamentoDTO();
                responsePagamentoDTO.setId(pagamento.getId());
                responsePagamentoDTO.setCartao_id(pagamento.getCartao().getId());
                responsePagamentoDTO.setDescricao(pagamento.getDescricao());
                responsePagamentoDTO.setValor(pagamento.getValor());

                responsePagamentoDTOList.add(responsePagamentoDTO);
            }

            return responsePagamentoDTOList;
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    @Transactional
    public void deletePagamentosById (int id_cartao) {

        Optional<Cartao> cartao = cartaoService.findCartaoById(id_cartao);
        if (cartao.isPresent()){
            pagamentoRepository.deleteAllByCartao(cartao.get());
        }else {
            throw new RuntimeException("Cartao nao cadastrado!");
        }
    }

    public List<Pagamento> findAllByCartao (int id_cartao) {
        Optional<Cartao> cartao = cartaoService.findCartaoById(id_cartao);

        if (cartao.isPresent()) {
            return pagamentoRepository.findAllByCartao(cartao.get());
        }
        return Collections.EMPTY_LIST;
    }
}

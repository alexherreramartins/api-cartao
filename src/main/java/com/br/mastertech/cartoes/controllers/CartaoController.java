package com.br.mastertech.cartoes.controllers;

import com.br.mastertech.cartoes.DTOs.RequestCartaoDTO;
import com.br.mastertech.cartoes.DTOs.ResponseCartaoDTO;
import com.br.mastertech.cartoes.DTOs.ResponseConsultaCartaoDTO;
import com.br.mastertech.cartoes.DTOs.StatusDTO;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao createCartao(@RequestBody @Valid RequestCartaoDTO requestCartaoDTO) {
        try {
            return cartaoService.createCartao(requestCartaoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("{id}")
    public ResponseCartaoDTO updateStatusCartao(@RequestBody @Valid StatusDTO statusDTO , @PathVariable(name = "id") int id ){
        try{
            return cartaoService.updateStatusCartao(statusDTO,id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseConsultaCartaoDTO findCartaoById(@PathVariable(name = "id") int id ){
        try{
            return cartaoService.findCartaoByIdDTO(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

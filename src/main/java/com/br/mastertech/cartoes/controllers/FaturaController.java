package com.br.mastertech.cartoes.controllers;

import com.br.mastertech.cartoes.DTOs.ResponseBloqueioDTO;
import com.br.mastertech.cartoes.DTOs.ResponseFaturaDTO;
import com.br.mastertech.cartoes.DTOs.ResponsePagamentoDTO;
import com.br.mastertech.cartoes.services.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    FaturaService faturaService;

    @GetMapping("/{cliente-id}/{cartao-id}")
    public Iterable<ResponsePagamentoDTO> findPagamentosIdClienteCartao(@PathVariable(name = "cliente-id") int cliente_id,
                                                                        @PathVariable(name = "cartao-id") int cartao_id) {
        try {
            return faturaService.findPagamentosIdClienteCartao(cliente_id, cartao_id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public ResponseFaturaDTO cadastrarPagamentoFatura(@PathVariable(name = "cliente-id") int cliente_id,
                                                           @PathVariable(name = "cartao-id") int cartao_id) {
        try {
            return faturaService.cadastrarPagamentoFatura(cliente_id, cartao_id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public ResponseBloqueioDTO bloquearCartaoCliente(@PathVariable(name = "cliente-id") int cliente_id,
                                                     @PathVariable(name = "cartao-id") int cartao_id) {
        try {
            return faturaService.bloquearCartaoCliente(cliente_id, cartao_id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

package com.br.mastertech.cartoes.services;

import com.br.mastertech.cartoes.DTOs.ResponseBloqueioDTO;
import com.br.mastertech.cartoes.DTOs.ResponseFaturaDTO;
import com.br.mastertech.cartoes.DTOs.ResponsePagamentoDTO;
import com.br.mastertech.cartoes.DTOs.StatusDTO;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.models.Cliente;
import com.br.mastertech.cartoes.models.Pagamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FaturaService {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;


    public Iterable<ResponsePagamentoDTO> findPagamentosIdClienteCartao(int id_cartao, int id_cliente) {

        Optional<Cliente> cliente = clienteService.findClienteById(id_cliente);

        if (cliente.isPresent()) {
            Optional<Cartao> cartao = cartaoService.findCartaoById(id_cartao);
            if (cartao.isPresent()) {
                if (cartao.get().getCliente().getId() == id_cliente) {
                    return pagamentoService.findPagamentosIdCartao(cartao.get().getId());
                }
                throw new RuntimeException("Cliente não é dono deste cartão!");
            }
            throw new RuntimeException("Cartão nao cadastrado na base!");
        }
        throw new RuntimeException("Cliente não possui cadastro no sistema.");
    }

    public ResponseFaturaDTO cadastrarPagamentoFatura(int cliente_id, int cartao_id) {

        Optional<Cliente> cliente = clienteService.findClienteById(cliente_id);

        if (cliente.isPresent()) {
            Optional<Cartao> cartao = cartaoService.findCartaoById(cartao_id);
            if (cartao.isPresent()) {
                if (cartao.get().getCliente().getId() == cliente_id) {
                    ResponseFaturaDTO responseFaturaDTO = new ResponseFaturaDTO();

                    responseFaturaDTO.setId(cartao.get().getId());
                    responseFaturaDTO.setPagoEm(new Date(System.currentTimeMillis()));
                    responseFaturaDTO.setValorPago(calculaValorFatura(cartao_id));

                    pagamentoService.deletePagamentosById(cartao.get().getId());

                    return responseFaturaDTO;

                }
                throw new RuntimeException("Cliente não é dono deste cartão!");
            }
            throw new RuntimeException("Cartão nao cadastrado na base!");
        }
        throw new RuntimeException("Cliente não possui cadastro no sistema.");
    }

    private double calculaValorFatura(int cartao_id) {

        double valorFinal = 0.0;
        List<Pagamento> pagamentoList = (List<Pagamento>) pagamentoService.findAllByCartao(cartao_id);

        if (pagamentoList.size() > 0) {
            for (Pagamento pagamento : pagamentoList) {
                valorFinal += pagamento.getValor();
            }


        }
        return valorFinal;

    }

    public ResponseBloqueioDTO bloquearCartaoCliente(int cliente_id, int cartao_id) {
        Optional<Cliente> cliente = clienteService.findClienteById(cliente_id);

        if (cliente.isPresent()) {
            Optional<Cartao> cartao = cartaoService.findCartaoById(cartao_id);
            if (cartao.isPresent()) {
                StatusDTO statusDTO = new StatusDTO();
                statusDTO.setAtivo(false);

                cartaoService.updateStatusCartao(statusDTO, cartao.get().getId());

                ResponseBloqueioDTO responseBloqueioDTO = new ResponseBloqueioDTO();
                responseBloqueioDTO.setStatus("ok");

                return responseBloqueioDTO;

            }
            throw new RuntimeException("Cliente não é dono deste cartão!");
        }
        throw new RuntimeException("Cartão nao cadastrado na base!");
    }

}

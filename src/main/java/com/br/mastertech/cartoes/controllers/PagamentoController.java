package com.br.mastertech.cartoes.controllers;

import com.br.mastertech.cartoes.DTOs.RequestPagamentoDTO;
import com.br.mastertech.cartoes.DTOs.ResponsePagamentoDTO;
import com.br.mastertech.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponsePagamentoDTO createPagamento(@RequestBody @Valid RequestPagamentoDTO pagamentoDTO ){
        try {
            return pagamentoService.createPagamento(pagamentoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("{id_cartao}")
    public Iterable<ResponsePagamentoDTO> findPagamentosIdCartao(@PathVariable(name = "id_cartao") int id_cartao){

        return pagamentoService.findPagamentosIdCartao(id_cartao);
    }

}

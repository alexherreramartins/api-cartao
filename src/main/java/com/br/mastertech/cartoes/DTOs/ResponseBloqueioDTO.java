package com.br.mastertech.cartoes.DTOs;

public class ResponseBloqueioDTO {
    private String status;

    public ResponseBloqueioDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

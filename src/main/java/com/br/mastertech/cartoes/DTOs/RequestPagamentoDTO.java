package com.br.mastertech.cartoes.DTOs;

import javax.validation.constraints.*;

public class RequestPagamentoDTO {

    private int cartao_id;

    @NotNull
    @NotBlank
    private String descricao;

    @DecimalMin(value = "0.01")
    private double valor;

    public RequestPagamentoDTO() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}

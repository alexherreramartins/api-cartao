package com.br.mastertech.cartoes.repositories;

import com.br.mastertech.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}

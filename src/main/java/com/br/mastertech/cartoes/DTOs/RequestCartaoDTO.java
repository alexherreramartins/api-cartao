package com.br.mastertech.cartoes.DTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RequestCartaoDTO {

    @NotBlank
    private String numero;
    @NotNull
    private int clienteId;

    public RequestCartaoDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}

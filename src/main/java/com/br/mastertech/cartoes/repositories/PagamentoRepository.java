package com.br.mastertech.cartoes.repositories;

import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento,Integer> {

    List<Pagamento> findAllByCartao(Cartao cartao);

    void deleteAllByCartao(Cartao cartao);
}
